import request from '@/utils/request' // axios
import story from '@/api/config/story.js' // url

/**
 * 根据景点ID查景点坐标
 */
export function dramaCoordinates(data) {
	return request({
		url: story.drama_coordinates,
		method: 'post',
		data
	})
}

/**
 * 剧情列表
 */
export function list(data) {
	return request({
		url: story.drama_list,
		method: 'get',
		params:data
	})
}

/**
 * 文字转语音
 */
export function textToAudio(data) {
	return request({
		url: story.drama_text,
		method: 'post',
		data: data,
		responseType: "arraybuffer" //后台返回的为流数据
	})
}

/**
 * 故事、剧情、景点坐标
 */
export function selectDramaDetail(data) {
	return request({
		url: story.drama_detail,
		method: 'get',
		params:data
	})
}
