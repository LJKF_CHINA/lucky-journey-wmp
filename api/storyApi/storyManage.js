import request from '@/utils/request' // axios
import story from '@/api/config/story.js' // url

/**
 * 故事列表
 */
export function list(data) {
	return request({
		url: story.story_list,
		method: 'get',
		params:data
	})
}
/**
 * 单个故事
 */
export function storyById(data) {
	return request({
		url: story.story_id,
		method: 'get',
		params:data
	})
}