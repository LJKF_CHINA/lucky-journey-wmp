import request from '@/utils/request' // axios
import xf from '@/api/config/xf.js' // url

/* 
	语音识别
	@param base64
	@return text
 
 */
export function audioToText(data) {
	return request({
		url: xf.iat_audioToText,
		method: 'post',
		data
	})
}

/* 
	语音输入返回大数据 + 合成语音结果
	@param base64
	@return map
 
 */
export function audioReply(data) {
	return request({
		url: xf.iat_audioReply,
		method: 'post',
		data
	})
}

/* 
	语音输入返回大数据结果
	@param base64
	@return text
 
 */
export function textReply(data) {
	return request({
		url: xf.iat_textReply,
		method: 'POST',
		data
	})
}


export const test = () => request.get("/xfiat/test")