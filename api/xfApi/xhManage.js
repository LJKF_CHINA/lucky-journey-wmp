import request from '@/utils/request' // axios
import xf from '@/api/config/xf.js' // url

/* 
	输入返回大数据 + 合成语音结果
	@param text
	@return map
 
 */
export function audioReply(params) {
	return request({
		url: xf.ai_audioReply,
		method: 'get',
		params
	})
}

/* 
	输入返回大数据结果
	@param text
	@return text
 
 */
export function textReply(params) {
	return request({
		url: xf.ai_textReply,
		method: 'get',
		params
	})
}