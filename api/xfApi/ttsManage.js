import request from '@/utils/request' // axios
import xf from '@/api/config/xf.js' // url

/* 
	音频转换
	@param base64
	@return text
 
 */
export function pcmToMp3(data) {
	return request({
		url: xf.tts_pcmToMp3,
		method: 'post',
		data: data,
		responseType: "arraybuffer" //后台返回的为流数据
	})
}