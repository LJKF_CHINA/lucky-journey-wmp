export default {
	//故事列表
	story_list: "/story/list",
	//单个故事
	story_id: "/story/selectStoryById",
	//故事、剧情、景点坐标
	drama_detail: "/drama/selectDramaDetail",
	//剧情列表
	drama_list: "/drama/list",
	//根据景点ID查景点坐标
	drama_coordinates: "/drama/coordinates",
	//文字转语音
	drama_text: "/xftts/textToAudio"
}