export default {
	// 语音转文字接口
	iat_audioToText: "/xfiat/baseToText",

	iat_test: "/xfiat/test",
	// 语音转文字 --》 Ai回答接口
	iat_textReply: '/xfiat/textReply',
	// 语音转文字 --》 Ai回答 --》语音合成接口
	iat_audioReply: '/xfiat/audioReply',
	// Ai回答接口
	ai_textReply: '/xfxh/sendByText',
	// Ai回答 --》语音合成接口
	ai_audioReply: '/xfxh/sendByAudio',
	// 音频转换接口
	tts_pcmToMp3:'/xftts/pcmToMp3',
}