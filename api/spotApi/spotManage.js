import request from '@/utils/request' // axios
import spotapi from '@/api/config/spot.js' // url

//获取营业的景点
export function businessList(id) {
	return request({
		url: spotapi.spotList + `/${id}`,
		method: 'get',
	})
}


export function getSpot(id) {
	return request({
		url: spotapi.getSpot + `/${id}`,
		method: 'get',
		// responseType: 'arraybuffer'
	})
}