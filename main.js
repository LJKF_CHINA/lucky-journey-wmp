import App from './App'

// #ifndef VUE3
import Vue from 'vue'
import uView from '@/uni_modules/uview-ui'
Vue.use(uView)
import './uni.promisify.adaptor'
Vue.config.productionTip = false
App.mpType = 'app'

import customTabBar from './custom-tab-bar/index.vue'
Vue.component('tab-bar',customTabBar)

// 1. 导入 store 的实例对象
import store from '@/store/index.js'


import hljs from 'highlight.js';

import 'highlight.js/styles/atom-one-dark.css' //样式

//创建v-highlight全局指令

Vue.directive('highlight', function(el) {

	let blocks = el.querySelectorAll('pre code');

	blocks.forEach((block) => {

		hljs.highlightBlock(block)

	})

})


const app = new Vue({
	...App,
	store,
})
app.$mount()

// #endif

// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
export function createApp() {
	const app = createSSRApp(App)
	return {
		app
	}
}
// #endif