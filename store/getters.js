const getters = {
	AiFloatingWindow: (state) => state.app.AiFloatingWindow,
	user: (state) => state.user.user,
	local: (state) => state.user.local,
	tabbarIndex: (state) => state.app.tabbarIndex,
}

export default getters;