// TODO: store数据
const state = {
	// Ai悬浮窗
	AiFloatingWindow: {
		opened: false,
		QAList: [],
	},
	tabbarIndex: 0,
	// ...
};
// TODO: 改变数据方法的集合,这里是同步请求
const mutations = {
	// 切换显示隐藏
	TOGGLE_AIFLOATINGWINDOW: state => {
		state.AiFloatingWindow.opened = !state.AiFloatingWindow.opened
	},
	// 记录问题
	RECODE_AIFLOATINGWINDOW: (state, QAList) => {
		state.AiFloatingWindow.QAList.push(QAList)
	},
	SET_TABBARINDEX: (state, index) => {
		state.tabbarIndex = index
	}

};
// TODO: 这里是异步请求
const actions = {
	toggleAiFloatingWindow({ commit }) {
		commit('TOGGLE_AIFLOATINGWINDOW')
	},
	saveQAList({ commit }, newQAList) {
		commit('RECODE_AIFLOATINGWINDOW', newQAList)
	},
	setTabbarIndex({ commit }, index) {
		console.log("store", index);
		commit('SET_TABBARINDEX', index)
	}
};

// TODO：挂载 store 模块
const modules = {};
// TODO:其它插件配置，比如持久化插件等
const plugins = [];

export default {
	namespaced: true,
	state,
	mutations,
	actions
}