// TODO: store数据
const state = {
	user: null,
	local: null,
};
// TODO: 改变数据方法的集合
const mutations = {
	// 存用户
	SET_USER: (state, payload) => state.user = payload,
	// 存位置信息
	SET_LOCAL: (state, address) => state.local = address,
	// 存头像
	SET_AVATARURL: (state, avatarUrl) => state.user.avatarUrl = avatarUrl,
};
// TODO: 异步请求
const actions = {
	/* 
		用户信息获取
	 */
	getUserInfo() {
		// 传入判断是否已经授权

		// 假如有，将用户授权的信息传给user
		// commit("SET_USER", userInfo);
		// 假如说没有，调用授权方法
		return false;
	},
	saveUserInfo({ commit }, user) {
		// 写请求和方法....
		return new Promise((resolve, reject) => {
			commit("SET_USER", user);
			resolve('success');
		})
	},
	/* 
	  用户信息授权
	 */
	authorize({ commit }) {
		uni.showModal({
			title: '提示',
			content: '亲，授权微信登录后才能正常使用小程序功能~',
			success: (resbtn) => {
				if (resbtn.confirm) {
					uni.getUserProfile({
						desc: '获取您的昵称、头像、地区及性别',
						success: res => {
							uni.showToast({
								title: "已获取成功！",
								icon: "success",
								duration: 1000
							});
							var userInfo = res.userInfo
							console.log(userInfo);
							commit("SET_USER", userInfo);
							return true;
						},
						fail: err => {
							console.log(err.errMsg)
							//拒绝授权
							uni.showToast({
								title: `授权已取消...,原因${err.errMsg}`,
								icon: 'error',
								duration: 2000
							});
							return false;
						}
					})
				} else if (resbtn.cancel) {
					//如果用户点击了取消按钮
					uni.showToast({
						title: '您拒绝了请求,部分功能将无法正常使用',
						icon: 'error',
						duration: 2000
					});
					return false;
				}
			}
		});
	},
	setLocal({ commit }) {
		// 获取当前地理位置信息
		uni.getLocation({
			type: 'wgs84',
			geocode: true,
			success: (res) => {
				console.log('当前位置的经度：' + res.longitude);
				console.log('当前位置的纬度：' + res.latitude);
				// 传到高德Api
				setTimeout(() => {
					let data = {
						address: '福州.仓山区',
						longitude: res.longitude,
						latitude: res.latitude
					}
					commit("SET_LOCAL", data);
				}, 500)
			},
			fail: (err) => {
				console.log(err);
			}
		});
	},
	chooseAvatar({ commit }, avatarUrl) {
		return new Promise((resolve) => {
			// 调用接口请求
			setTimeout(() => {
				commit('SET_AVATARURL', avatarUrl)
				uni.showToast({
					title: '修改成功!',
					icon: 'success',
					duration: 2000
				});
				resolve();
			}, 200)
		})
	}
};




// TODO：挂载 store 模块
const modules = {

};
// TODO:其它插件配置，比如持久化插件等
const plugins = [];

export default {
	namespaced: true,
	state,
	mutations,
	actions
}