// 获得顶部 + 标签栏适配高度
export function getNavIconDistance() {
	// 获取手机顶部状态栏的高度
	const systemInfo = uni.getSystemInfoSync();
	// 获取手机顶部状态栏的高度
	const statusBarHeight = systemInfo.statusBarHeight || 0;
	// 获取导航栏的高度（手机状态栏高度 + 胶囊高度 + 胶囊的上下间距）
	const menuButtonInfo = uni.getMenuButtonBoundingClientRect();
	const navBarHeight = menuButtonInfo.height + (menuButtonInfo.top - statusBarHeight) *
		2;
	// 计算顶部图标距离
	const topIconDistance = statusBarHeight + navBarHeight + 5;
	return topIconDistance;
}
// 获得底部适配高度
export function getBarIconDistance() {
	// 打印底部安全适配距离
	const systemInfo = uni.getSystemInfoSync();
	const tabBarHeight = systemInfo.screenHeight - systemInfo.safeArea.bottom;
	return tabBarHeight;
}